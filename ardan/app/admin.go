package main

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	uuid "github.com/satori/go.uuid"
	"io/ioutil"
	"net/http"
)

type Admin struct {
	ID    uuid.UUID `json:"id"`
	Login string    `json:"login"`
	Pass  string    `json:"pass"`
}

type Session struct {
	Login string `json:"login"`
	Token string `json:"token"`
}

func GetAdminFromRequest(w http.ResponseWriter, r *http.Request) Admin {
	body, err := ioutil.ReadAll(r.Body)
	ReturnInternalError(w, err)
	var admin Admin
	err = json.Unmarshal(body, &admin)
	ReturnInternalError(w, err)
	return admin
}

func (a *Admin) CreateAdmin() error {
	ID := uuid.NewV4()
	sum := sha256.Sum256([]byte(a.Pass))
	hash := hex.EncodeToString(sum[:])
	admin := Admin{ID: ID, Login: a.Login, Pass: hash}
	err := WriteAdmin(admin)
	return err
}

func (a *Admin) isAdmin() bool {
	sum := sha256.Sum256([]byte(a.Pass))
	pass := hex.EncodeToString(sum[:])
	admin, err := GetAdmin(a.Login)
	OnErr(err)
	return admin.Pass == pass
}
