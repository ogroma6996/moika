package main

import (
	"github.com/gorilla/mux"
	"net/http"
)

func main() {
	// InitDB
	InitDB()

	// Router
	r := mux.NewRouter()

	// Urls
	r.HandleFunc("/create_admin", SessionMiddleware(CreateAdminHandler)).Methods(http.MethodPost)
	r.HandleFunc("/login", LoginHandler)
	r.HandleFunc("/", SessionMiddleware(IndexHandler)).Methods(http.MethodGet)

	// Server
	err := http.ListenAndServe(":8080", r)
	OnErr(err)
}
