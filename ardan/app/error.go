package main

import (
	"fmt"
	"log"
	"net/http"
)

func OnErr(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

func OnErrFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func ReturnInternalError(w http.ResponseWriter, err error) {
	if err != nil {
		OnErr(err)
		w.WriteHeader(http.StatusInternalServerError)
		_, err = w.Write([]byte(fmt.Sprintf("Error: %s", err)))
		return
	}
}
