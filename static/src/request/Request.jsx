import Axios from "axios";
/*import { navigate } from "hookrouter";*/
import endpoint from "../config";

export function errorHandler(err, url, payload, options = {}) {
  if (err.status === 401) {
    localStorage.removeItem("role");
    localStorage.removeItem("loggedIn");
    localStorage.removeItem("user");
    localStorage.removeItem("access_token");
    /*    navigate("/", true);*/
    window.location.reload();
  }
  console.log(
    `${err.status ? err.status : "Error fetching data"}: ${
      err.statusText
    } - ${url}`,
    err,
    payload
  );
  if (!options.preventError) {
    throw err;
  }
  throw err;
}

function handleResponse(res) {
  if (!res.ok) {
    return Promise.reject(res);
  }
  return res.json();
}

export const getRequest = async (url, header) => {
  let authorize;
  if (!!localStorage.getItem("access_token")) {
    authorize = {
      Authorization: `Bearer ${localStorage.getItem("access_token")}`,
    };
  }
  const data = Axios.get(endpoint + url, {
    headers: authorize,
  })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      let validErr = err.response ? err.response : err;
      errorHandler(validErr, url);
    });
  return data;
};

export const postRequest = async (url, body) => {
  let authorize;
  if (!!localStorage.getItem("access_token")) {
    authorize = {
      Authorization: `Bearer ${localStorage.getItem("access_token")}`,
    };
  }
  const params = { ...body };

  const data = Axios.post(endpoint + url, params, {
    headers: authorize,
  }).then((res) => {
    return res;
  });
  return data;
};

export const postFileRequest = async (url, body) => {
  let authorize;
  if (!!localStorage.getItem("access_token")) {
    authorize = {
      Authorization: `Bearer ${localStorage.getItem("access_token")}`,
    };
  }
  // const params = { ...body };

  const data = Axios.post(endpoint + url, body, {
    headers: {
      ...authorize,
      "Content-Type": "multipart/form-data",
    },
  })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      let validErr = err.response ? err.response : err;
      errorHandler(validErr, url);
    });
  return data;
};

export const putRequest = async (url, body) => {
  let authorize;
  if (!!localStorage.getItem("access_token")) {
    authorize = {
      Authorization: `Bearer ${localStorage.getItem("access_token")}`,
    };
  }
  const params = { ...body };

  const data = Axios.put(endpoint + url, params, {
    headers: {
      ...authorize,
    },
  })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      let validErr = err.response ? err.response : err;
      errorHandler(validErr, url);
    });
  return data;
};

export const deleteRequest = async (url) => {
  let authorize;
  if (!!localStorage.getItem("access_token")) {
    authorize = {
      Authorization: `Bearer ${localStorage.getItem("access_token")}`,
    };
  }
  let params = {
    method: "DELETE",
    headers: {
      ...authorize,
    },
  };
  const data = await fetch(`${endpoint + url}`, params)
    .then((res) => {
      return handleResponse(res);
    })
    .catch((err) => {
      let validErr = err.response ? err.response : err;
      errorHandler(validErr, url);
    });
  return data;
};
