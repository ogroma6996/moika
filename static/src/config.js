const config = {
  baseUrl: "https://cid636.core12.ru",
  version: "v1",
  apiName: "api",
};

const { baseUrl, version, apiName } = config;

const endpoint = `${baseUrl}/${apiName}/${version}`;

export default endpoint;
