import "./App.scss";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import Home from "./pages/Home";
import { createContext, useReducer } from "react";
import Authorize from "./pages/autorization/Authorize";
import { appReducer, initialState } from "./AppReducer";

export const AppContext = createContext({});

function App() {
  const [state, dispatch] = useReducer(appReducer, initialState);
  let value = { state, dispatch };

  const switchRouter = (authorize) => {
    switch (authorize) {
      case "active":
        document.body.classList.remove("login");
        return <Home />;
      default:
        document.body.classList.add("login");
        return <Authorize />;
    }
  };

  return (
    <AppContext.Provider value={value}>
      <div className="App">{switchRouter(state.authorize)}</div>
    </AppContext.Provider>
  );
}

export default App;
