import React, { useState } from "react";
import { listObjects } from "./ListObject";
import { Ul } from "./style";

const Objects = ({ setDateCheckbox }) => {
  const [dataCheckbox, setDataCheckbox] = useState(() => {
    const obj = {};
    listObjects.forEach((e) => {
      obj[e.title] = e.check;
    });
    return obj;
  });

  const handleCheck = (e) => {
    const { name, checked, value, type } = e.target;
    setDataCheckbox({
      ...dataCheckbox,
      [name]: type === "checkbox" ? checked : value,
    });
    setDateCheckbox({
      ...dataCheckbox,
      [name]: type === "checkbox" ? checked : value,
    });
  };

  const items = () => {
    return listObjects.map((e) => {
      return (
        <li key={e.id}>
          <label>
            <input
              type="checkbox"
              checked={dataCheckbox[e.title]}
              defaultChecked={e.check}
              name={e.title}
              onChange={handleCheck}
            />{" "}
            {e.title}
          </label>
        </li>
      );
    });
  };

  return (
    <>
      <Ul>{items()}</Ul>
    </>
  );
};

export default Objects;
