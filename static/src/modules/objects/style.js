import styled from "styled-components";

export const Ul = styled.ul`
  margin: 0;
  padding: 0;
  li {
    display: inline-block;
    list-style: none;
    margin: 0 10px 0 0;
    label {
      margin: 0;
      cursor: pointer;
    }
  }
`;
