import React, { useState } from "react";
import { DateRangePicker } from "react-dates";
import moment from "moment";
moment.locale("ru");

const RangePicker = ({ date }) => {
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [focusedInput, setFocusedInput] = useState(null);
  const handleDatesChange = ({ startDate, endDate }) => {
    date({
      startDate,
      endDate,
    });
    setStartDate(startDate);
    setEndDate(endDate);
  };

  return (
    <DateRangePicker
      startDate={startDate}
      startDateId="tata-start-date"
      endDate={endDate}
      isOutsideRange={() => null}
      endDateId="tata-end-date"
      startDatePlaceholderText="От"
      endDatePlaceholderText="До"
      hideKeyboardShortcutsPanel={true}
      onDatesChange={handleDatesChange}
      focusedInput={focusedInput}
      onFocusChange={(focusedInput) => setFocusedInput(focusedInput)}
    />
  );
};

export default RangePicker;
