import styled from "styled-components";

export const SiteBarMenuNew = styled.ul`
  /*  margin: 75px -32px 0 0;*/
  margin: 35px -32px 0 0;
  padding: 0;
  list-style: none;
  text-align: left;
  & li.active {
    a {
      color: #10182fcc;
      background-color: #fafafb;
      border-radius: 50px 0 0 50px;
      transition: 0.3s;
      &:after {
        background-color: #2b7a78;
      }
    }
  }
  a {
    position: relative;
    display: inline-block;
    width: 100%;
    padding: 12px 12px 12px 54px;
    font-weight: 600;
    font-size: 16px;
    line-height: 20px;
    color: #feffff;
    text-decoration: none;
    &:hover {
      color: #10182fcc;
      background-color: #fafafb;
      border-radius: 50px 0 0 50px;
      transition: 0.3s;
      transition-timing-function: ease-in-out;
      &:after {
        transition: 0.3s;
        transition-timing-function: ease-in-out;
        background-color: #2b7a78;
      }
    }
    &:after {
      content: "";
      position: absolute;
      left: 14px;
      top: 50%;
      display: inline-block;
      width: 16px;
      height: 16px;
      border-radius: 50%;
      transform: translateY(-50%);
      background-color: #def2f1;
    }
  }
`;
