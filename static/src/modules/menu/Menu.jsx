import React, { useMemo } from "react";
import MenuItem from "./MenuItem";
import { SiteBarMenuNew } from "./style";

const Menu = () => {
  const list = useMemo(
    () => [
      {
        id: 1,
        title: "Главная",
        href: "",
      },
      {
        id: 2,
        title: "Общая статистика",
        href: "statistics",
      },
      {
        id: 3,
        title: "Статистика по объектам",
        href: "statistics-object",
      },
      {
        id: 4,
        title: "Demo",
        href: "demo",
      },
    ],
    []
  );

  return (
    <SiteBarMenuNew>
      <MenuItem data={list} />
    </SiteBarMenuNew>
  );
};

export default Menu;
