import React from "react";
import { A } from "hookrouter";

const MenuItem = ({ data }) => {
  const location = window.location.pathname.split("/")[1];
  const menu = () => {
    return data.map((item) => {
      return (
        <li key={item.id} className={item.href === location ? "active" : null}>
          <A href={`/${item.href}`}>{item.title}</A>
        </li>
      );
    });
  };

  return menu();
};

export default MenuItem;
