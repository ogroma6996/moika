export const dayList = [
  "Январь",
  "Февраль",
  "Март",
  "Апрель",
  "Май",
  "Июнь",
  "Июль",
  "Август",
  "Сентябрь",
  "Октябрь",
  "Ноябрь",
  "Декабрь",
];

export const categoriesTime = () => {
  const arr = [];
  for (let i = 8; i < 23; i++) {
    arr.push(`${i}:00`);
  }
  return arr;
};

export const dataChart = (width, max, firm) => {
  const dataDay = [];
  for (let i = 0; i < firm; i++) {
    const dataTime = [];
    for (let j = 0; j < width; j++) {
      dataTime.push(Math.floor(Math.random() * max));
    }
    dataDay.push({
      name: i % 2 === 0 ? "Наличные" : "Карточка",
      data: dataTime,
    });
  }
  return dataDay;
};

export const dataMonth = (width, max, firm) => {
  const obj = [];
  for (let i = 0; i < firm; i++) {
    const dataDay = [];
    obj.push({ name: i % 2 === 0 ? "Наличные" : "Карточка" });
    for (let j = 0; j < width; j++) {
      dataDay.push(Math.floor(Math.random() * max));
    }
    obj[i].data = dataDay;
  }
  return obj;
};

export const day = () => {
  const arrDay = [];
  for (let i = 1; i < 32; i++) {
    arrDay.push(i);
  }

  return arrDay;
};

export const dataChart2 = (width, max) => {
  const dataDay = [];
  for (let i = 1; i < width; i++) {
    dataDay.push(Math.floor(Math.random() * max));
  }
  return dataDay;
};

export const dataMonthCompany = (width, max, firm) => {
  const obj = [];
  for (let i = 0; i < firm; i++) {
    const dataDay = [];
    obj.push({ name: i % 2 === 0 ? "Фирма 1" : "Фирма 2" });
    for (let j = 0; j < width; j++) {
      dataDay.push(Math.floor(Math.random() * max));
    }
    obj[i].data = dataDay;
  }
  return obj;
};
