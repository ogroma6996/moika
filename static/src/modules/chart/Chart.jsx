import React, { useEffect, useState } from "react";
import ReactApexChart from "react-apexcharts";
import { categoriesTime } from "../demo/Demo";
import { animateScroll as scroll } from "react-scroll";

const Chart = ({
  title,
  title2,
  title3,
  categories,
  colors,
  page,
  dataChart,
  dataMonth,
  dataChartTime,
  stacked,
  dataLabels,
}) => {
  const [visible, setVisible] = useState(false);
  const [visibleTime, setVisibleTime] = useState(false);
  const [data, setData] = useState({
    series: dataChart,
    options: {
      chart: {
        type: "bar",
        height: 350,
        stacked: stacked?.stacked1 || false,
        toolbar: {
          show: true,
        },
        zoom: {
          enabled: true,
        },
        fontFamily: "Roboto, sans-serif",
        events: {
          dataPointSelection: function () {
            setVisible(false);
            setVisibleTime(false);
            setVisible((e) => !e);
            setTimeout(() => {
              scroll.scrollToBottom();
            }, 100);
          },
        },
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: "55%",
          endingShape: "rounded",
        },
      },
      dataLabels: {
        enabled: true,
      },
      stroke: {
        show: true,
        width: 2,
        colors: ["transparent"],
      },
      xaxis: {
        categories,
      },
      yaxis: {
        title: {
          text: title,
        },
      },
      fill: {
        opacity: 1,
      },
      tooltip: {
        y: {
          formatter: function (val) {
            return `${val} руб.`;
          },
        },
      },
    },
  });

  useEffect(() => {
    setData({
      ...data,
      series: dataChart,
    });
    /*
        if (dataMonth) {
      setData({
        ...data,
        series: dataMonth,
      });
    }
*/

    if (colors) {
      setData({
        ...data,
        options: {
          colors: colors,
        },
      });
    }
  }, [colors, dataChart]);

  const closeWindow = (lv) => {
    if (lv === "2") {
      setVisible(false);
      setVisibleTime(false);
    } else {
      setVisibleTime(false);
    }
  };
  const wrapperToday = () => {
    return (
      <ReactApexChart
        options={data.options}
        series={data.series}
        type="bar"
        height={350}
      />
    );
  };

  const arrDay = [];
  for (let i = 1; i < 32; i++) {
    arrDay.push(i);
  }
  const [dataTwo, setDataTwo] = useState({
    ...data,
    series: dataMonth /*[
      {
        name: "Выручка",
        data: dataChart2(32, 10000),
      },
    ],*/,
    options: {
      chart: {
        stacked: stacked?.stacked2 || false,
        events: {
          dataPointSelection: function (e, chart, opts) {
            if (dataChartTime) {
              setVisibleTime(false);
              setVisibleTime((e) => !e);
              setTimeout(() => {
                scroll.scrollToBottom();
              }, 100);
            }
          },
        },
      },
      /*colors: ["#00e396"],*/
      xaxis: {
        categories: arrDay,
      },
      yaxis: {
        title: {
          text: title2,
        },
      },
      tooltip: {
        y: {
          formatter: function (val) {
            return `${val}руб.`;
          },
        },
      },
    },
  });
  const [dataTree, setDataTree] = useState({
    series: dataChartTime,
    options: {
      chart: {
        stacked: stacked?.stacked3 || false,
      },
      xaxis: {
        categories: categoriesTime(),
      },
      yaxis: {
        title: {
          text: title3,
        },
      },

      tooltip: {
        y: {
          formatter: function (val) {
            return `${val} руб.`;
          },
        },
      },
    },
  });
  const wrapperStatistic = () => {
    return (
      <>
        <ReactApexChart
          options={data.options}
          series={data.series}
          type="bar"
          height={350}
        />
        {visible && (
          <>
            <br />
            <br />
            <h4 className="text-center">Отчет по суткам</h4>
            <div className="additionChart">
              <span className="btn" onClick={() => closeWindow("2")}>
                Закрыть статистику
              </span>
              <ReactApexChart
                options={dataTwo.options}
                series={dataTwo.series}
                type="bar"
                height={350}
              />
            </div>
          </>
        )}
        {visibleTime && (
          <>
            <br />
            <br />
            <h4 className="text-center">Отчет по часам</h4>
            <div className="additionChart">
              <span className="btn" onClick={() => closeWindow("3")}>
                Закрыть статистику
              </span>
              <ReactApexChart
                options={dataTree.options}
                series={dataTree.series}
                type="bar"
                height={350}
              />
            </div>
          </>
        )}
      </>
    );
  };

  /*-----------*/
  const demoData = {
    series: dataChart,
    options: {
      dataLabels: {
        enabled: dataLabels,
      },
      chart: {
        type: "bar",
        height: 350,
        stacked: stacked?.stacked1 || false,
        toolbar: {
          show: true,
        },
        zoom: {
          enabled: true,
        },
        fontFamily: "Roboto, sans-serif",
        events: {
          dataPointSelection: function (e, b, c) {
            setDemoDataTwo({
              ...demoDataTwo,
              options: {
                dataLabels: {
                  enabled: dataLabels,
                },
                chart: {
                  stacked: stacked?.stacked2 || false,
                },
                xaxis: {
                  categories: dataMonth[c.dataPointIndex].month,
                },
                yaxis: {
                  title: {
                    text: title2,
                  },
                },
                tooltip: {
                  y: {
                    formatter: function (val) {
                      return `${val}руб.`;
                    },
                  },
                },
              },
              series: dataMonth[c.dataPointIndex].data,
            });
            setVisible(false);
            setVisibleTime(false);
            setVisible((e) => !e);
            setTimeout(() => {
              scroll.scrollToBottom();
            }, 100);
          },
        },
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: "55%",
          endingShape: "rounded",
        },
      },
      stroke: {
        show: true,
        width: 2,
        colors: ["transparent"],
      },
      xaxis: {
        categories,
      },
      yaxis: {
        title: {
          text: title,
        },
      },
      fill: {
        opacity: 1,
      },
      tooltip: {
        y: {
          formatter: function (val) {
            return `${val} руб.`;
          },
        },
      },
    },
  };
  const [demoDataTwo, setDemoDataTwo] = useState({
    ...demoData,
    series: [],
    options: {
      dataLabels: {
        enabled: dataLabels,
      },
      chart: {
        stacked: stacked?.stacked2 || false,
      },
      xaxis: {
        categories: [],
      },
      yaxis: {
        title: {
          text: title2,
        },
      },
      tooltip: {
        y: {
          formatter: function (val) {
            return `${val}руб.`;
          },
        },
      },
    },
  });
  const demo = () => {
    return (
      <>
        <ReactApexChart
          options={demoData.options}
          series={demoData.series}
          type="bar"
          height={350}
        />
        {visible && (
          <>
            <br />
            <br />
            <h4 className="text-center">Отчет по суткам</h4>
            <div className="additionChart">
              <span className="btn" onClick={() => closeWindow("2")}>
                Закрыть статистику
              </span>
              <ReactApexChart
                options={demoDataTwo.options}
                series={demoDataTwo.series}
                type="bar"
                height={350}
              />
            </div>
          </>
        )}
      </>
    );
  };
  /*-----------*/

  const returnPage = () => {
    switch (page) {
      case "a3":
        return wrapperStatistic();
      case "demo":
        return demo();
      default:
        return wrapperToday();
    }
  };

  return returnPage();
};

export default Chart;
