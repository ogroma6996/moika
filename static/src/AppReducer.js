export const AUTHORIZE = 0;

export const initialState = {
  authorize: localStorage.getItem("authorize"),
};

export const appReducer = (state, action) => {
  switch (action.type) {
    case AUTHORIZE:
      return { ...state, authorize: action.payload };
  }
};
