import React, { useEffect, useState } from "react";
import { Box, Title } from "../styles/defaultStyle";
import { dataMonth, dayList } from "../../modules/demo/Demo";
import StatisticsChart from "./StatisticsChart";

const Statistics = () => {
  const [date, setDate] = useState({});

  useEffect(() => {
    if (!!date) {
      return false;
    }
  }, [date]);

  return (
    <>
      <Title>Общая статистика</Title>
      <Box>
        <StatisticsChart
          title="Общая статистика"
          title2="Статистика по суткам"
          title3="Статистика по часам"
          setDate={setDate}
          categories={dayList}
          dataChart={dataMonth(12, 10000, 2)}
          dataMonth={dataMonth(31, 300, 2)}
          dataChartTime={dataMonth(15, 10000, 2)}
          page="a3"
          stacked={{
            stacked1: true,
            stacked2: true,
            stacked3: true,
          }}
        />
      </Box>
    </>
  );
};

export default Statistics;
