import React from "react";
import RangePicker from "../../modules/rangePicker/RangePicker";
import Chart from "../../modules/chart/Chart";

const StatisticsChart = ({
  setDate,
  categories,
  stacked,
  dataChart,
  dataChartTime,
  dataMonth,
  page,
  visibleCalendar,
  dataLabels,
}) => {
  return (
    <>
      {visibleCalendar && <RangePicker date={setDate} />}
      <hr />
      <Chart
        title="Общая статистика"
        title2="Статистика по суткам"
        title3="Статистика по часам"
        categories={categories}
        stacked={stacked}
        dataLabels={dataLabels}
        dataMonth={dataMonth}
        dataChart={dataChart}
        dataChartTime={dataChartTime}
        page={page}
      />
    </>
  );
};

export default StatisticsChart;
