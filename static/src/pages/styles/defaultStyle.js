import styled from "styled-components";

export const Title = styled.h6`
  margin: 0 0 30px;
  font-size: 38px;
`;

export const Box = styled.div`
  padding: 15px;
  border-radius: 6px;
  box-shadow: 0 0 20px #a0a0a0;
`;

export const Logo = styled.div`
  color: #fff;
  font-size: 40px;
  line-height: 1;
  padding-bottom: 35px;
  border-bottom: 1px solid #fff;
`;
