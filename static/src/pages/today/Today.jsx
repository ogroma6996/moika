import React from "react";
import { Box, Title } from "../styles/defaultStyle";
import Chart from "../../modules/chart/Chart";
import { categoriesTime, dataChart } from "../../modules/demo/Demo";

const Today = () => {
  return (
    <>
      <Title>За сегодня</Title>
      <Box>
        <Chart
          title="Выручка за сутки"
          categories={categoriesTime()}
          stacked={{
            stacked1: true,
            stacked2: true,
            stacked3: true,
          }}
          dataChart={dataChart(15, 5000, 2)}
        />
      </Box>
    </>
  );
};

export default Today;
