import styled from "styled-components";
import { Form } from "react-bootstrap";

export const NewForm = styled(Form)`
  position: absolute;
  width: 400px;
  padding: 25px;
  left: 50%;
  top: 50%;
  border-radius: 6px;
  transform: translate(-50%, -50%);
  background-color: #fff;
  color: #5d5d5d;
`;
