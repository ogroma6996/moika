import { A } from "hookrouter";
import React from "react";
import "./style";

const NotFound = () => {
  return (
    <div className="test">
      <h1>404 страница не найдена</h1>
      <A href="/">Домой</A>
    </div>
  );
};

export default NotFound;
