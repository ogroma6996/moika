import React, { useContext, useEffect } from "react";
import "moment/locale/ru";
import Statistics from "./statistics/Statistics";
import StatisticsObject from "./statisticsObject/StatisticsObject";
import { Col, Row } from "react-bootstrap";
import NotFound from "./not-found/NotFound";
import Today from "./today/Today";
import Menu from "../modules/menu/Menu";
import { useRedirect, useRoutes } from "hookrouter";
import { AppContext } from "../App";
import Demo from "./Demo";
import { Logo } from "./styles/defaultStyle";

const Home = () => {
  const { dispatch } = useContext(AppContext);
  useRedirect("/sign-in", "/");

  const routes = {
    "/": () => <Today />,
    "/statistics": () => <Statistics />,
    "/statistics-object": () => <StatisticsObject />,
    "/demo": () => <Demo />,
  };

  const routeResult = useRoutes(routes);

  /*  useEffect(() => {
    const formData = new FormData();
    formData.append("_username", 89250108558);
    formData.append("_password", 20200428);
    fetch("/login_check", {
      method: "POST",
      body: formData,
      headers: {
        cookie: "PHPSESSID=4u0l55ubi3jmiouchbkar5c925",
      },
    });
  }, []);*/

  return (
      <>
        <div className="container-fluid">
          <Row>
            <Col xl={2} md={3} className="sidebar">
              <Logo>
                <img src="./logo-mini.png" alt="" /> LOGO
              </Logo>
              <Menu />
            </Col>
            <Col
                xl={{ span: 10, offset: 2 }}
                md={{ span: 9, offset: 3 }}
                className="mrt"
            >
              {routeResult || <NotFound />}
            </Col>
          </Row>
        </div>
      </>
  );
};

export default Home;
