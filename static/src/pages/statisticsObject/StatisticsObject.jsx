import React, { useState } from "react";
import { Box, Title } from "../styles/defaultStyle";
import Objects from "../../modules/objects/Objects";
import StatisticsChart from "../statistics/StatisticsChart";
import { dataMonth, dayList } from "../../modules/demo/Demo";

const StatisticsObject = () => {
  const [date, setDate] = useState({});
  const [test, setTest] = useState(dataMonth(12, 10000, 3));

  const setDateCheckbox = (e) => {
    const width = 12;
    const max = 10000;
    const key = Object.keys(e);
    const company = key.length;
    const arr = [];

    for (let i = 0; i < company; i++) {
      if (e[key[i]]) {
        const obj = {};
        const dataDay = [];
        obj.name = key[i];
        for (let j = 0; j < width; j++) {
          dataDay.push(Math.floor(Math.random() * max));
        }
        obj.data = dataDay;
        arr.push(obj);
      }
    }

    setTest(arr);
  };

  return (
    <>
      <Title>Статистика по объектам</Title>
      <Box>
        <Objects setDateCheckbox={setDateCheckbox} />
        <hr />
        <StatisticsChart
          title="Общая статистика"
          title2="Статистика по суткам"
          title3="Статистика по часам"
          setDate={setDate}
          categories={dayList}
          dataChart={test}
          dataMonth={dataMonth(31, 1300, 3)}
          dataChartTime={dataMonth(15, 10000, 2)}
          page="a3"
        />
      </Box>
    </>
  );
};

export default StatisticsObject;
