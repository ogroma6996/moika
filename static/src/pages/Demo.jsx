import React from "react";
import { Box, Title } from "./styles/defaultStyle";
import StatisticsChart from "./statistics/StatisticsChart";
import { demoData } from "../modules/demo/DemoData";

const Demo = () => {
  const categories = [];

  const test1 = () => {
    const obj = [
      {
        name: "Наличные",
        data: [],
      },
      {
        name: "Карточка",
        data: [],
      },
    ];
    demoData.map((e, i) => {
      let profit = 0;
      let total_all = 0;
      let total_card = 0;
      let total_cash = 0;

      categories.push(e.month);
      e.data.map((day) => {
        profit += day.profit;
        day.stations.map((total) => {
          total_all += total.total;
          total_card += total.total_card;
          total_cash += total.total_cash;
        });
      });

      obj[0].data.push(total_card);
      obj[1].data.push(total_cash);
    });

    return obj;
  };
  const test2 = () => {
    const arrDay = [];

    demoData.map((e, i) => {
      const demo1 = [];
      const demo2 = [];
      const dayList = [];

      e.data.map((day) => {
        let total_all = 0;
        let total_card = 0;
        let total_cash = 0;
        dayList.push(day.date);
        if (day.stations.length) {
          day.stations.map((total) => {
            total_all += total.total;
            total_card += total.total_card;
            total_cash += total.total_cash;
          });
          demo1.push(total_card);
          demo2.push(total_cash);
        } else {
          demo1.push(0);
          demo2.push(0);
        }
      });

      arrDay.push({
        month: dayList,
        data: [
          {
            name: "Наличные",
            data: demo1,
          },
          {
            name: "Карточка",
            data: demo2,
          },
        ],
      });
    });
    return arrDay;
  };

  return (
    <>
      <Title>Общая статистика</Title>
      <Box>
        <StatisticsChart
          title="Общая статистика"
          title2="Статистика по суткам"
          categories={categories}
          dataChart={test1()}
          dataMonth={test2()}
          page="demo"
          visibleCalendar={false}
          dataLabels={false}
          stacked={{
            stacked1: true,
            stacked2: true,
          }}
        />
      </Box>
    </>
  );
};

export default Demo;
