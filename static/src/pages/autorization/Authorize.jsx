import React from "react";
import { useRedirect, useRoutes } from "hookrouter";
import SingIn from "./SingIn";
import NotFound from "../not-found/NotFound";

const Authorize = () => {
  // redirect
  useRedirect("/", "/sign-in");

  const routes = {
    "/": () => <SingIn />,
    "/sign-in": () => <SingIn />,
  };
  const routeResult = useRoutes(routes);

  return <>{routeResult || <NotFound />}</>;
};

export default Authorize;
