import React, { useContext, useEffect } from "react";
import { Button, Container, Form } from "react-bootstrap";
import { NewForm } from "../not-found/style";
import { AppContext } from "../../App";
import { AUTHORIZE } from "../../AppReducer";

const SingIn = () => {
  const { dispatch } = useContext(AppContext);

  const handleSubmit = (e) => {
    e.preventDefault();
    localStorage.setItem("authorize", "active");
    dispatch({
      type: AUTHORIZE,
      payload: "active",
    });
  };

  return (
    <>
      <Container>
        <NewForm>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Логин</Form.Label>
            <Form.Control type="text" placeholder="Введите логин" />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Пароль</Form.Label>
            <Form.Control type="password" placeholder="Password" />
          </Form.Group>
          <div className="text-center">
            <Button variant="danger" type="submit" onClick={handleSubmit}>
              Войти
            </Button>
          </div>
        </NewForm>
      </Container>
    </>
  );
};

export default SingIn;
