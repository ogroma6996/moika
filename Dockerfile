FROM node:14

WORKDIR /usr/src/app
COPY static/ .
RUN npm install
COPY . .
EXPOSE 8080
CMD [ "npm", "start"]